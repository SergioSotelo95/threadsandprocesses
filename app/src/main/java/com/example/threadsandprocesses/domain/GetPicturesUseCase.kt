package com.example.threadsandprocesses.domain

import com.example.threadsandprocesses.data.PictureRepository
import com.example.threadsandprocesses.data.model.PictureModel
import retrofit2.Response

class GetPicturesUseCase {
    private val repository = PictureRepository()
    suspend operator fun invoke(): List<PictureModel> = repository.getAllPictures()
}