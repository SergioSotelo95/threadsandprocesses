package com.example.threadsandprocesses.domain

import com.example.threadsandprocesses.data.PictureRepository
import com.example.threadsandprocesses.data.model.PictureModel

class GetSinglePictureUseCase {
        private val repository = PictureRepository()
        suspend operator fun invoke(int: Int): PictureModel = repository.getOnePicture(int)

}