package com.example.threadsandprocesses.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.threadsandprocesses.R
import com.example.threadsandprocesses.core.PicturesAPP
import com.example.threadsandprocesses.data.model.PictureModel
import com.example.threadsandprocesses.data.network.PictureListService
import com.example.threadsandprocesses.databinding.ActivityMainBinding
import com.example.threadsandprocesses.ui.adapter.PictureListAdapter
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var pictureListAdapter: PictureListAdapter
    private val pictureListService = PictureListService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()


        lifecycleScope.launch {
            binding.ProgressBar.isVisible = true
            val response = try {
                pictureListService.getPictures()
            } catch (e: IOException) {
                Log.e(TAG, "IOException")
                binding.ProgressBar.isVisible = false
                return@launch
            } catch (e: HttpException) {
                Log.e(TAG, "HttpException, unexpected response")
                binding.ProgressBar.isVisible = false
                return@launch
            }

            if (!response.isNullOrEmpty()) {
                pictureListAdapter.allPictures = response
                (applicationContext as PicturesAPP).room.picturesDao().insert(response)
                refresher(response)
            } else {
                Log.e(TAG, "Response not successful")
            }

            binding.ProgressBar.isVisible = false
        }
    }

    private fun setupRecyclerView() = binding.RV.apply {
        pictureListAdapter = PictureListAdapter()
        adapter = pictureListAdapter
        layoutManager = LinearLayoutManager(this@MainActivity)
    }

    private suspend fun refresher(obj: List<PictureModel>) {
        val swipe: SwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        swipe.setOnRefreshListener {
            swipe.isRefreshing = false
        }
        (applicationContext as PicturesAPP).room.picturesDao().insert(obj)
    }
}