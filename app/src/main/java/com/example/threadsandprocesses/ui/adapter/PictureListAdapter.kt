package com.example.threadsandprocesses.ui.adapter


import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.threadsandprocesses.R
import com.example.threadsandprocesses.ui.view.ImageActivity
import com.example.threadsandprocesses.data.model.PictureModel
import com.example.threadsandprocesses.databinding.ImageListCardBinding

class PictureListAdapter : RecyclerView.Adapter<PictureListAdapter.PictureListHolder>() {

    inner class PictureListHolder(val binding: ImageListCardBinding) : RecyclerView.ViewHolder(binding.root)

    private val diffCallback = object : DiffUtil.ItemCallback<PictureModel>() {
        override fun areItemsTheSame(oldItem: PictureModel, newItem: PictureModel): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: PictureModel, newItem: PictureModel): Boolean {
            return oldItem == newItem
        }
    }
    private val differ = AsyncListDiffer(this, diffCallback)

    var allPictures: List<PictureModel>
        get() = differ.currentList
        set(value) {
            differ.submitList(value)
        }

    override fun getItemCount(): Int = allPictures.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PictureListHolder {
        return PictureListHolder(
            ImageListCardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PictureListHolder, position: Int) {
        holder.binding.apply {
            val picture = allPictures[position]
            ImageTitle.text = picture.title
            val thumbnailURL = picture.thumbnailUrl + ".jpg"
            Glide.with(ThumbnailLayout.context).load(thumbnailURL).placeholder(R.drawable.error).into(Thumbnail)
            ThumbnailLayout.setOnClickListener {
                val intentImage = Intent(ThumbnailLayout.context, ImageActivity::class.java)
                intentImage.putExtra("id", picture.id)
                startActivity(ThumbnailLayout.context, intentImage, null)
            }
        }
    }



}