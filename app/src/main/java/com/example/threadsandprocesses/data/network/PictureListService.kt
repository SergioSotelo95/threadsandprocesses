package com.example.threadsandprocesses.data.network

import com.example.threadsandprocesses.core.RetrofitHelper
import com.example.threadsandprocesses.data.model.PictureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PictureListService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getPictures(): List<PictureModel> {
        val numberList = mutableListOf<Int>()
        for (number in (1..25)){
            numberList.add(number)
        }
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(PictureApiClient::class.java).getCustomPictures(numberList)
            response.body() ?: emptyList()
        }
    }
}