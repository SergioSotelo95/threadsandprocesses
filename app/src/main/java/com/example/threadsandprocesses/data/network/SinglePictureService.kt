package com.example.threadsandprocesses.data.network

import com.example.threadsandprocesses.core.RetrofitHelper
import com.example.threadsandprocesses.data.model.PictureModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SinglePictureService() {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getSinglePicture(id: Int): PictureModel {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(PictureApiClient::class.java).get1Picture(id)
            response.body()!!
        }
    }
    }
