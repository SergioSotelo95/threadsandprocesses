package com.example.threadsandprocesses.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.example.threadsandprocesses.data.model.PictureModel

@Dao
interface PicturesDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert (picturesList: List<PictureModel>)
}