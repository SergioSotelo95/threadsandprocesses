package com.example.threadsandprocesses.data

import com.example.threadsandprocesses.data.model.PictureModel
import com.example.threadsandprocesses.data.model.PictureProvider
import com.example.threadsandprocesses.data.model.SinglePictureProvider
import com.example.threadsandprocesses.data.network.PictureListService
import com.example.threadsandprocesses.data.network.SinglePictureService

class PictureRepository {
    private val api = PictureListService()
    private val singlePictureApi = SinglePictureService()

    suspend fun getAllPictures(): List<PictureModel> {
        val response = api.getPictures()
        PictureProvider.pictures = response
        return response
    }
    suspend fun getOnePicture(int: Int): PictureModel {
        val response = singlePictureApi.getSinglePicture(int)
        SinglePictureProvider.picture = response
        return response
    }


}