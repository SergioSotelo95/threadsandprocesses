package com.example.threadsandprocesses.core

import android.app.Application
import androidx.room.Room
import com.example.threadsandprocesses.data.database.PicturesDataBase

class PicturesAPP: Application() {
    val room by lazy {
        Room
            .databaseBuilder(this, PicturesDataBase::class.java, "pictures")
            .build()
    }
}